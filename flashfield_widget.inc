<?php

/**
 * @file
 * flashfield widget hooks and callbacks.
 */

/**
 * Implementation of CCK's hook_widget_settings($op = 'form').
 */
function flashfield_widget_settings_form($widget) {

  $form = module_invoke('filefield', 'widget_settings', 'form', $widget);

  // Sizes
  $form['size_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Size settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 8,
  );

  $form['size_settings']['custom_size'] = array(
    '#type' => 'checkbox',
    '#title' => t('Custom sizes'),
    '#default_value' =>  !empty($widget['custom_size']) ? $widget['custom_size'] : 0,
    '#description' => t('Allow users to specify a custom height and width for their content.'),
  );
  
  $form['size_settings']['max_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum height'),
    '#default_value' => !empty($widget['max_height']) ? $widget['max_height'] : '',
    '#size' => 15,
    '#maxlength' => 10,
    '#description' => t('The displayed flash height - content will be scaled to comply.'),
  );

  $form['size_settings']['max_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum width'),
    '#default_value' => !empty($widget['max_width']) ? $widget['max_width'] : '',
    '#size' => 15,
    '#maxlength' => 10,
    '#description' => t('The displayed flash width - content will be scaled to comply.'),
  );
  
  // Alt
  $form['alt_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Alternate text settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 8,
  );
  $form['alt_settings']['custom_alt'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable custom alternate text'),
    '#default_value' =>  !empty($widget['custom_alt']) ? $widget['custom_alt'] : 0,
    '#description' => t('Enable user input alternate text for images.'),
  );
  $form['alt_settings']['alt'] = array(
    '#type' => 'textfield',
    '#title' => t('Default alternate text'),
    '#default_value' => !empty($widget['alt']) ? $widget['alt'] : '!default',
    '#description' => t('This value will be used for alternate text by default.'),
    '#suffix' => theme('token_help', 'file'),
  );

  // Flashvars
  $form['flashvars_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Flashvars settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 8,
  );
  $form['flashvars_settings']['custom_flashvars'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable custom flashvars'),
    '#default_value' =>  !empty($widget['custom_flashvars']) ? $widget['custom_flashvars'] : 0,
    '#description' => t('Allow users to enter custom flashvars.'),
  );
  
  // Base
  $form['base_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Base settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 8,
  );
  $form['base_settings']['custom_base'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable custom base'),
    '#default_value' =>  !empty($widget['custom_base']) ? $widget['custom_base'] : 0,
    '#description' => t('Allow users to customize the base path.'),
  );
  $form['base_settings']['base'] = array(
    '#type' => 'textfield',
    '#title' => t('Default base'),
    '#default_value' => !empty($widget['base']) ? $widget['base'] : '',
    '#description' => t('Leave blank to use the default value of the system files\' directory.'),
  );
  
  // Params
  $form['params_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Params settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 8,
  );
  $form['params_settings']['custom_params'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable custom parameters'),
    '#default_value' =>  !empty($widget['custom_params']) ? $widget['custom_params'] : 0,
    '#description' => t('Allow users to customize the flash parameters.'),
  );
  
  $form['params_settings']['params'] = array(
    '#type' => 'textfield',
    '#title' => t('Default parameters'),
    '#default_value' => !empty($widget['params']) ? $widget['params'] : '',
    '#description' => t('This value will be used for alternate text by default.'),
  );

  return $form;
}

/**
 * Element specific validation for flashfield default value.
 *
 * Validated in a separate function from flashfield_field() to get access
 * to the $form_state variable.
 */
function _flashfield_widget_settings_default_validate($element, &$form_state) {
  // Verify the destination exists
  $destination = file_directory_path() .'/flashfield_default_images';
  if (!field_file_check_directory($destination, FILE_CREATE_DIRECTORY)) {
    form_set_error('default_image', t('The default image could not be uploaded. The destination %destination does not exist or is not writable by the server.', array('%destination' => dirname($destination))));
    return;
  }

  $validators = array(
    'file_validate_is_image' => array(),
  );

  // We save the upload here because we can't know the correct path until the file is saved.
  if (!$file = file_save_upload('default_image_upload', $validators, $destination)) {
    // No upload to save we hope... or file_save_upload() reported an error on its own.
    return;
  }

  // Remove old image (if any) & clean up database.
  $old_default = $form_state['values']['default_image'];
  if (!empty($old_default['fid'])) {
    if (file_delete(file_create_path($old_default['filepath']))) {
      db_query('DELETE FROM {files} WHERE fid=%d', $old_default['fid']);
    }
  }

  // Make the file permanent and store it in the form.
  file_set_status($file, FILE_STATUS_PERMANENT);
  $file->timestamp = time();
  $form_state['values']['default_image'] = (array)$file;
 }

/**
 * Implementation of CCK's hook_widget_settings($op = 'validate').
 */
function flashfield_widget_settings_validate($widget) {

//  // Check that only web images are specified in the callback.
//  $extensions = array_filter(explode(' ', $widget['file_extensions']));
//  $web_extensions = array('swf', 'flv', 'mp3' ,'jpg', 'jpeg', 'gif', 'png');
//  if (count(array_diff($extensions, $web_extensions))) {
//    form_set_error('file_extensions', t('Only swf, flv, mp3 and web-standard images (jpg, gif, and png) are supported through the flash widget. If needing to upload other types of images, change the widget to use a standard file upload.'));
//  }

//  // Check that set resolutions are valid.
//  foreach (array('min_resolution', 'max_resolution') as $resolution) {
//    if (!empty($widget[$resolution]) && !preg_match('/^[0-9]+x[0-9]+$/', $widget[$resolution])) {
//      form_set_error($resolution, t('Please specify a resolution in the format WIDTHxHEIGHT (e.g. 640x480).'));
//    }
//  }

  // TODO - Validate max height and width
  
}

/**
 * Implementation of CCK's hook_widget_settings($op = 'save').
 */
function flashfield_widget_settings_save($widget) {
  $filefield_settings = module_invoke('filefield', 'widget_settings', 'save', $widget);
  return array_merge($filefield_settings, array('custom_size', 'max_height', 'max_width', 'alt',  'custom_alt', 'custom_flashvars', 'base', 'custom_base', 'params', 'custom_params'));
}

/**
 * Element #value_callback function.
 */
function flashfield_widget_value($element, $edit = FALSE) {
  
  
  $item = filefield_widget_value($element, $edit);

  if ($edit) {
    $item['height'] = isset($edit['height']) ? $edit['height'] : '';
    $item['width'] = isset($edit['width']) ? $edit['width'] : '';
    $item['alt'] = isset($edit['alt']) ? $edit['alt'] : '';
    $item['flashvars'] = isset($edit['flashvars']) ? $edit['flashvars'] : '';
    $item['base'] = isset($edit['base']) ? $edit['base'] : '';
    $item['params'] = isset($edit['params']) ? $edit['params'] : '';
  }
  else {
    $item['height'] = '';
    $item['width'] = '';
    $item['alt'] = '';
    $item['flashvars'] = '';
    $item['base'] = '';
    $item['params'] = '';
  }
  
  return $item;
}

/**
 * Element #process callback function.
 */
function flashfield_widget_process($element, $edit, &$form_state, $form) {
  
//  dsm('Processing');
//  dsm($element);
  
  $file = $element['#value'];
  $field = content_fields($element['#field_name'], $element['#type_name']);

  // Assign default height and width - TODO implement flashnode mechanism to handle case when this isn't visible?
  if (isset($element['preview']) && $element['#value']['fid'] != 0) {

    // Get info
    $info = image_get_info($file['filepath']);
    
    // Assign height
    if (((empty($file['data']['height']) && $file['data']['height'] !== '0') ||  $field['widget']['custom_size']) && $info) {
      dsm('Setting height');
      $file['data']['height'] = $info['height'];
    }
    else {
      $file['data']['height'] = '';
    }
    
    // Assign width
    if (((empty($file['data']['width']) && $file['data']['width'] !== '0') ||  $field['widget']['custom_size'])  && $info) {
      $file['data']['width'] = $info['width'];
    }
    else {
      $file['data']['width'] = '';
    }
  }
  
  $element['data']['height'] = array(
    '#type' => $field['widget']['custom_size'] ? 'textfield' : 'value',
    '#title' => t('Height'),
    '#default_value' => $file['data']['height'],
    '#description' => t('Height.'),
  );
  
  // #value must be hard-coded if #type = 'value'.
  if (!$field['widget']['custom_size']) {
    $element['data']['height']['#value'] = $file['data']['height'];
  }

  $element['data']['width'] = array(
    '#type' => $field['widget']['custom_size'] ? 'textfield' : 'value',
    '#title' => t('Width'),
    '#default_value' => $file['data']['width'],
    '#description' => t('Width.'),
  );
  
  // #value must be hard-coded if #type = 'value'.
  if (!$field['widget']['custom_size']) {
    $element['data']['width']['#value'] = $file['data']['width'];
  }

  $element['data']['alt'] = array(
    '#title' => t('Alternate text'),
    '#type' => $field['widget']['custom_alt'] ? 'textfield' : 'value',
    '#default_value' => $file['data']['alt'],
    '#description' => t('This text will be used by screen readers, search engines, or when the image cannot be loaded.'),
  );

  // #value must be hard-coded if #type = 'value'.
  if (!$field['widget']['custom_alt']) {
    $element['data']['alt']['#value'] = $field['widget']['alt'];
  }
  
  $element['data']['flashvars'] = array(
    '#type' => $field['widget']['custom_flashvars'] ? 'textfield' : 'value',
    '#title' => t('Flashvars'),
    '#default_value' => $file['data']['flashvars'],
    '#description' => t('Flashvars.'),
  );
  
  // #value must be hard-coded if #type = 'value'.
  if (!$field['widget']['custom_flashvars']) {
    $element['data']['flashvars']['#value'] = '';
  }
  
  $element['data']['base'] = array(
    '#type' => $field['widget']['custom_base'] ? 'textfield' : 'value',
    '#title' => t('Base'),
    '#default_value' => $file['data']['base'],
    '#description' => t('Base.'),
  );
  
  // #value must be hard-coded if #type = 'value'.
  if (!$field['widget']['custom_base']) {
    $element['data']['base']['#value'] = $field['widget']['base'];
  }
  
  $element['data']['params'] = array(
    '#type' => $field['widget']['custom_params'] ? 'textfield' : 'value',
    '#title' => t('Params'),
    '#default_value' => $file['data']['params'],
    '#description' => t('Params.'),
  );
  
  // #value must be hard-coded if #type = 'value'.
  if (!$field['widget']['custom_params']) {
    $element['data']['params']['#value'] = $field['widget']['params'];
  }

  return $element;
}

/**
 * FormAPI theme function. Theme the output of an image field.
 */
function theme_flashfield_widget(&$element) {
//  drupal_add_css(drupal_get_path('module', 'flashfield') .'/flashfield.css');
  return theme('form_element', $element, $element['#children']);
}


/**
 * widget_validate().
 */
function flashfield_widget_validate(&$element, &$form_state) {
  dsm('Validating');
//  dsm('Element');
//  dsm($element);
//  dsm('Form state');
  dsm($form_state);
//

  if (!empty($element['data']['height']['#value']) && !is_numeric($element['data']['height']['#value'])) {
    form_set_error($element['#field_name'] . '][' . $element['#delta'] . '][data][height', t('Height must be a number, or blank.'));
  }
  
  if (!empty($element['data']['width']['#value']) && !is_numeric($element['data']['width']['#value'])) {
    form_set_error($element['#field_name'] . '][' . $element['#delta'] . '][data][width', t('Width must be a number, or blank.'));
  }
  
}
